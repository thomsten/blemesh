# This file is part of blemesh.
#
# blemesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blemesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with blemesh.  If not, see <http://www.gnu.org/licenses/>.

from Crypto.Cipher import AES
from Crypto.Hash import CMAC


def s1(m):
    m = bytes(m)
    cipher = CMAC.new(b'\x00'*16, ciphermod=AES).update(m)
    return bytearray(cipher.digest())


def k1(n, salt, p):
    n, salt, p = bytes(n), bytes(salt), bytes(p)
    t = CMAC.new(salt, ciphermod=AES).update(n).digest()
    return bytearray(CMAC.new(t, ciphermod=AES).update(p).digest())


def k2(n, p):
    n, p = bytes(n), bytes(p)
    salt = bytes(s1(b'smk2'))
    t = CMAC.new(salt, ciphermod=AES).update(n).digest()
    t0 = b''
    t1 = CMAC.new(t, ciphermod=AES).update(t0 + p + b'\x01').digest()
    t2 = CMAC.new(t, ciphermod=AES).update(t1 + p + b'\x02').digest()
    t3 = CMAC.new(t, ciphermod=AES).update(t2 + p + b'\x03').digest()
    result = bytearray((t1 + t2 + t3)[-33:])
    result[0] = result[0] & 0x7F
    return result


def k3(n):
    n = bytes(n)
    salt = bytes(s1(b'smk3'))
    t = CMAC.new(salt, ciphermod=AES).update(n).digest()
    return bytearray(CMAC.new(t, ciphermod=AES).update(b'id64' + b'\x01').digest()[-8:])


def k4(n):
    n = bytes(n)
    salt = bytes(s1(b'smk4'))
    t = CMAC.new(salt, ciphermod=AES).update(n).digest()
    result = CMAC.new(t, ciphermod=AES).update(b'id6' + b'\x01').digest()
    result = bytearray([result[-1]])[0] & 0x3f
    return result
