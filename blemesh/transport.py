# This file is part of blemesh.
#
# blemesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blemesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with blemesh.  If not, see <http://www.gnu.org/licenses/>.

import logging
import struct


class SarTXSession(object):
    def __init__(self, meta, upper_transport_pdu):
        pass


class SarRXSession(object):
    def __init__(self, meta, lower_transport_pdu):
        pass

    def rx_seg(self, meta):
        pass


class Transport(object):
    PDU_LENGTH_MAX = 384
    SEG_PDU_LENGTH_MAX = 12
    UNSEG_PDU_LENGTH_MAX = 15

    def __init__(self, mesh, name="Transport"):
        self._mesh = mesh

        self._name = name
        self._net = None
        self._access = None
        self._logger = logging.getLogger(self._name)

        self.sars = []

    def _net_tx(self, meta, pdu):
        if self._net:
            self._net.tx(meta, pdu)
        else:
            self._logger.info("net tx: %s, %r", pdu.hex(), meta)

    def _access_rx(self, meta, pdu):
        if self._access:
            self._access.rx(meta, pdu)
        else:
            self._logger.info("access rx: %s, %r", pdu.hex(), meta)

    def decrypt(self, meta, upper_transport_pdu):
        if meta["akf"]:
            for key in self._mesh.appkeys:
                if key.aid == meta["aid"]:
                    cleartext = key.decrypt(meta, upper_transport_pdu)
                    if cleartext:
                        return cleartext, key
        elif not meta["akf"] and meta["aid"] == 0x00:
            for key in self._mesh.devkeys:
                if key.src == meta["src"] or key.src == meta["dst"]:
                    cleartext = key.decrypt(meta, upper_transport_pdu)
                    if cleartext:
                        return cleartext, key

        self._logger.debug("Not able to decrypt PDU: \"%s\", %r",
                           upper_transport_pdu.hex(), meta)
        return None, None

    def rx_seg(self, meta, lower_transport_pdu):
        pass

    def rx_ctl(self, meta, upper_transport_pdu):
        pass

    def rx_access(self, meta, upper_transport_pdu):
        cleartext, key = self.decrypt(meta, upper_transport_pdu)
        if cleartext and key:
            meta["appkey"] = key
            self._access_rx(meta, cleartext)
        else:
            self._logger.debug("Not able to decrypt %s", upper_transport_pdu.hex())

    def rx(self, meta, lower_transport_pdu):
        header = lower_transport_pdu[0]
        meta["seg"] = (header >> 7) & 0x01
        meta["akf"] = (header >> 6) & 0x01
        meta["aid"] = header & 0x3f
        if meta["seg"] == 0:
            meta["mic_len"] = 4
        upper_transport_pdu = lower_transport_pdu[1:]
        self._logger.debug("meta %r, upper_transport_pdu: %s", meta, upper_transport_pdu.hex())
        if meta["seg"]:
            # Segmented access or transport control
            self.rx_seg(meta, upper_transport_pdu)
        elif meta["ctl"]:
            # Unsegmented transport control
            self.rx_ctl(meta, upper_transport_pdu)
        else:
            # Unsegmented access message
            self.rx_access(meta, upper_transport_pdu)

    def tx(self, meta, upper_transport_pdu):
        if ((len(upper_transport_pdu) <= Transport.PDU_LENGTH_MAX and
             meta["mic_len"] > 4)):
            raise ValueError("Transport MIC has to be 4 bytes for unsegmented messages.")

        meta["seq"] = self._mesh.seq
        meta["iv_index"] = self._mesh.iv_index

        if "devkey" in meta:
            meta["akf"] = 0
            ciphertext, mac = meta["devkey"].encrypt(meta, upper_transport_pdu)
        elif "appkey" in meta:
            meta["akf"] = 1
            ciphertext, mac = meta["appkey"].encrypt(meta, upper_transport_pdu)
        else:
            raise ValueError("Appkey or devkey not specified.")

        upper_transport_pdu = ciphertext + mac
        self.lower_tx(meta, upper_transport_pdu)

    def lower_tx(self, meta, upper_transport_pdu):
        if len(upper_transport_pdu) <= Transport.UNSEG_PDU_LENGTH_MAX:
            meta["seg"] = 0
            if meta["akf"] == 1:
                pdu = struct.pack(">B",
                                  (1 << 6) | (meta["appkey"].aid & 0x3f))
            else:
                pdu = struct.pack(">B", 0)
            pdu += upper_transport_pdu
            self._net_tx(meta, bytearray(pdu))
        else:
            self._tx_sar(metda, pdu)

