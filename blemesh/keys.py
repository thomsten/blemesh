# This file is part of blemesh.
#
# blemesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blemesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with blemesh.  If not, see <http://www.gnu.org/licenses/>.

from Crypto.Cipher import AES
from Crypto.Util.strxor import strxor
import struct
import blemesh.kdf as kdf
import logging


class NetworkKey(object):
    def __init__(self, key):
        self.logger = logging.getLogger("NetworkKey")
        self.key = key
        out = kdf.k2(key, b'\x00')
        self.nid = out[0]
        self.encryption_key = out[1:17]
        self.privacy_key = out[17:]
        self.network_id = kdf.k3(self.key)
        self.identity_key = kdf.k1(self.key, kdf.s1(b"nkik"), b"id128" + b"\x01")
        self.beacon_key = kdf.k1(self.key, kdf.s1(b"nkbk"), b"id128" + b"\x01")

    def __repr__(self):
        return self.key.hex()

    def obfuscate(self, meta, pdu):
        return self.deobfuscate(meta, pdu)

    def deobfuscate(self, meta, pdu):
        """Attempts to deobfuscate the network PDU header (CTL, TTL, SEQ, SRC)"""
        privacy_random = pdu[7:(7+7)]
        pecb_input = (bytes(5)
                      + struct.pack(">I", meta["iv_index"])
                      + privacy_random)
        pecb = AES.new(self.privacy_key, mode=AES.MODE_ECB).encrypt(pecb_input)
        self.logger.debug("Privacy Random: %s", privacy_random.hex())
        self.logger.debug("PECB Input: %s", pecb_input.hex())
        self.logger.debug("PECB: %s", pecb[0:6].hex())
        return bytes([pdu[0]]) + strxor(pdu[1:7], pecb[0:6]) + pdu[7:]

    def nonce_get(self, meta, pdu):
        # Ref. Mesh Profile spec v1.0 table 3.45
        # Exploit the PDU layout according to table 3.7
        return bytearray(bytes(1)
                         + pdu[1:(1 + 1 + 3 + 2)]
                         + bytes(2)
                         + struct.pack(">I", meta["iv_index"]))

    def decrypt(self, meta, pdu):
        """Decrypts a mesh PDU"""
        nonce = self.nonce_get(meta, pdu)
        if (pdu[1] & 0x80) > 0:
            mic_len = 8
        else:
            mic_len = 4

        MIN_NETWORK_LEN = 10
        if (len(pdu) - MIN_NETWORK_LEN - mic_len) < 0:
            return None

        ciphertext = pdu[7:-mic_len]
        mac = pdu[-mic_len:]
        ccm = AES.new(bytes(self.encryption_key),
                      mode=AES.MODE_CCM,
                      nonce=bytes(nonce),
                      mac_len=mic_len,
                      msg_len=len(ciphertext),
                      assoc_len=0)
        try:
            cleartext = ccm.decrypt_and_verify(bytes(ciphertext), bytes(mac))
            return bytearray(pdu[0:7] + cleartext + mac)
        except ValueError as e:
            # MAC check failed
            self.logger.debug("MAC check failed: %s", mac.hex())
            return None

    def encrypt(self, meta, pdu):
        """Encrypt a mesh PDU. Assumes the PDU is pre-filled."""
        nonce = self.nonce_get(meta, pdu)
        ccm = AES.new(bytes(self.encryption_key),
                      mode=AES.MODE_CCM,
                      nonce=bytes(nonce),
                      mac_len=meta["mic_len"],
                      msg_len=len(pdu[7:]),
                      assoc_len=0)
        ciphertext, mac = ccm.encrypt_and_digest(bytes(pdu[7:]))
        return bytearray(pdu[:7] + ciphertext + mac)


class ApplicationKey(object):
    def __init__(self, key):
        self.logger = logging.getLogger("ApplicationKey")
        self.key = key
        self.aid = kdf.k4(self.key)

    def __repr__(self):
        return self.key.hex()

    def nonce_get(self, meta):
        aszmic = b'\x00' if meta["mic_len"] == 4 else b'\0x01'
        nonce = (b'\x01'
                 + aszmic
                 + struct.pack(">I", meta["seq"])[1:]
                 + struct.pack(">HH", meta["src"], meta["dst"])
                 + struct.pack(">I", meta["iv_index"]))
        return bytearray(nonce)

    def decrypt(self, meta, transport_pdu):
        if meta["ctl"]:
            raise ValueError("Tried decrypting a transport layer control "
                             + "message with an application key.")
        elif self.aid != meta["aid"]:
            raise ValueError("Tried decrypting with invalid AID")
        elif not meta["akf"]:
            raise ValueError("Tried decrypting with application key "
                             + "for message encrypted with device key")

        ciphertext = transport_pdu[:-meta["mic_len"]]
        mac = transport_pdu[-meta["mic_len"]:]
        nonce = self.nonce_get(meta)
        ccm = AES.new(bytes(self.key),
                      mode=AES.MODE_CCM,
                      nonce=bytes(nonce),
                      mac_len=meta["mic_len"],
                      msg_len=len(ciphertext),
                      assoc_len=0)
        try:

            return bytearray(ccm.decrypt_and_verify(bytes(ciphertext), bytes(mac)))
        except ValueError as e:
            return None
        else:
            return None

    def encrypt(self, meta, transport_pdu):
        if meta["ctl"]:
            raise ValueError("Tried encrypting a transport layer control "
                             + "message with an application key.")

        nonce = self.nonce_get(meta)
        ccm = AES.new(bytes(self.key),
                      mode=AES.MODE_CCM,
                      nonce=bytes(nonce),
                      mac_len=meta["mic_len"],
                      msg_len=len(transport_pdu),
                      assoc_len=0)
        ciphertext, mic = ccm.encrypt_and_digest(bytes(transport_pdu))
        return bytearray(ciphertext), bytearray(mic)



class DeviceKey(object):
    def __init__(self, key, src):
        self.key = key
        self.src = src

    def __repr__(self):
        return self.key.hex()

    def nonce_get(self, meta):
        aszmic = b'\x00' if meta["mic_len"] == 4 else b'\0x01'
        nonce = (b'\x02'
                 + aszmic
                 + struct.pack(">I", meta["seq"])[1:]
                 + struct.pack(">HH", meta["src"], meta["dst"])
                 + struct.pack(">I", meta["iv_index"]))
        return bytearray(nonce)

    def decrypt(self, meta, transport_pdu):
        if meta["ctl"]:
            raise ValueError("Tried decrypting a transport layer control "
                             + "message with a device key.")
        elif 0x00 != meta["aid"]:
            raise ValueError("Tried decrypting with invalid AID != 0x00 for device keys.")
        elif meta["akf"]:
            raise ValueError("Tried decrypting with device key "
                             + "for message encrypted with application key")

        ciphertext = transport_pdu[:-meta["mic_len"]]
        mac = transport_pdu[-meta["mic_len"]:]
        nonce = self.nonce_get(meta)
        ccm = AES.new(bytes(self.key),
                      mode=AES.MODE_CCM,
                      nonce=bytes(nonce),
                      mac_len=meta["mic_len"],
                      msg_len=len(ciphertext),
                      assoc_len=0)
        try:
            self.logger.debug("Ciphertext: %s, TransMIC: %s", ciphertext.hex(), mac.hex())
            return bytearray(ccm.decrypt_and_verify(bytes(ciphertext), bytes(mac)))
        except ValueError as e:
            return None
        else:
            return None

    def encrypt(self, meta, transport_pdu):
        if meta["ctl"]:
            raise ValueError("Tried encrypting a transport layer control "
                             + "message with a device key.")
        elif 0x00 != meta["aid"]:
            raise ValueError("Tried encrypting with invalid AID")
        elif meta["akf"]:
            raise ValueError("Tried encrypting with device key "
                             + "for message encrypted with application key")

        nonce = self.nonce_get(meta)
        ccm = AES.new(bytes(self.key),
                      mode=AES.MODE_CCM,
                      nonce=bytes(nonce),
                      mac_len=meta["mic_len"],
                      msg_len=len(transport_pdu),
                      assoc_len=0)
        ciphertext, mic =  ccm.encrypt_and_digest(bytes(transport_pdu))
        return bytearray(ciphertext), bytearray(mic)
