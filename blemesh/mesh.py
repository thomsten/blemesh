# This file is part of blemesh.
#
# blemesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blemesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with blemesh.  If not, see <http://www.gnu.org/licenses/>.
import keys
from network import Network
from transport import Transport
from access import Access
import logging


class Mesh(object):
    SEQ_LIMIT = 2**22

    def __init__(self, appkeys, devkeys, netkeys, name="BT Mesh", seq=0, iv_index=0):
        logformat = "%(asctime)s %(filename)+10s:%(lineno)-4d -- %(message)s"
        logging.basicConfig(format=logformat)
        logging.StreamHandler(sys.stdout).setFormatter(logformat)

        self.logger = logging.getLogger('')
        self.logger.setLevel(logging.DEBUG)
        self._appkeys = appkeys
        self._devkeys = devkeys
        self._netkeys = netkeys
        self._seq = seq
        self._iv_index = iv_index

        self.net = Network(self)
        self.trs = Transport(self)
        self.access = Access(self)

        self.trs._net = self.net
        self.trs._access = self.access
        self.net._trs = self.trs
        self.net.tx_if = self._tx_if

    def iv_index_get(self, meta, pdu):
        # Check the IVI bit to see if we're using the current or previous IV index.

        return iv_index

    @property
    def iv_index(self, ivi=None):
        if (ivi is not None and (self._iv_index & 0x01 != ivi)):
            return self._iv_index - 1
        else:
            return self._iv_index

    @property
    def seq(self):
        seq = self._seq
        self._seq += 1
        if self._seq > self.SEQ_LIMIT:
            raise RuntimeError("FIXME: Update IV index before seqnum rollover")
        return seq

    @property
    def appkeys(self):
        return self._appkeys

    @property
    def devkeys(self):
        return self._devkeys

    @property
    def netkeys(self):
        return self._netkeys

    def tx(self, meta, pdu):
        meta["ctl"] = 0
        meta["src"] = 0x1201
        meta["dst"] = 0xffff
        self.trs.tx(meta, pdu)

    def _rx_cb(self, meta, pdu):
        self.logger.debug("RX: %s %r", pdu.hex(), meta)

    def _tx_if(self, meta, pdu):
        self.logger.debug("TX: %s %r", pdu.hex(), meta)
        self.net.rx(meta, pdu)


if __name__ == "__main__":
    # From the Mesh Profile v1.0 sample data #19
    appkey = keys.ApplicationKey(bytes.fromhex("63964771734fbd76e3b40519d1d94a48"))
    netkey = keys.NetworkKey(bytes.fromhex("7dd7364cd842ad18c17c2b820c84c3d6"))
    iv_index = 0x12345678
    m = Mesh(
        [appkey],
        [],
        [netkey],
        iv_index=iv_index, seq=9)

    meta= {"appkey": appkey, "netkey": netkey, "mic_len": 4, "ttl": 3, "seg": 1}
    m.tx(meta, bytes.fromhex("04000000010703"))
    print("Done")
