# This file is part of blemesh.
#
# blemesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blemesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with blemesh.  If not, see <http://www.gnu.org/licenses/>.

import logging
import struct


class Network(object):
    NETWORK_PDU_LENGTH_MIN = 15
    LOWER_TRANSPORT_PDU_LENGTH_MIN = 2
    LOWER_TRANSPORT_PDU_LENGTH_MAX = 16
    def __init__(self, mesh, name="Network"):
        self._mesh = mesh
        self._name = name
        self._logger = logging.getLogger("." + self._name)
        self._logger.setLevel(logging.DEBUG)
        self._trs = None

    def decrypt(self, meta, pdu):
        nid = pdu[0] & 0x7f
        for key in self._mesh.netkeys:
            if key.nid == nid:
                pdu_deobfuscated = key.deobfuscate(meta, pdu)
                cleartext = key.decrypt(meta, pdu_deobfuscated)
                if cleartext is not None:
                    return cleartext, key
        self._logger.debug("Not able to decrypt PDU: \"%s\"", pdu.hex())
        return None, None

    def rx(self, meta, pdu):
        if not isinstance(pdu, bytes):
            raise TypeError("PDU not of bytes type %s", type(pdu))
        elif len(pdu) < Network.NETWORK_PDU_LENGTH_MIN:
            self._logger.debug("Dropping too short message (%d)", len(pdu))

        meta = {}
        meta["iv_index"] = self._mesh.iv_index
        pdu, key = self.decrypt(meta, pdu)
        if pdu:
            meta["ivi"] = (pdu[0] >> 7) & 0x01
            meta["nid"] = (pdu[0] & 0x7f)
            meta["ctl"] = (pdu[1] >> 7) & 0x01
            meta["ttl"] = (pdu[1] & 0x7f)
            meta["seq"] = ((pdu[2] << 16) | (pdu[3] << 8) | pdu[4])
            meta["src"] = ((pdu[5] << 8) | pdu[6])
            meta["dst"] = ((pdu[7] << 8) | pdu[8])
            meta["netkey"] = key

            self._logger.debug("RX: %s, meta: %r", pdu.hex(), meta)
            self._trs.rx(meta, pdu[9:-4])

    def tx(self, meta, lower_transport_pdu):
        if not isinstance(lower_transport_pdu, bytes):
            raise TypeError("PDU not of bytes() type")
        elif (len(lower_transport_pdu) < Network.LOWER_TRANSPORT_PDU_LENGTH_MIN or
              len(lower_transport_pdu) > Network.LOWER_TRANSPORT_PDU_LENGTH_MAX):
            raise ValueError("Invalid lower transport PDU length (%d)".format(len(lower_transport_pdu)))

        pdu = b''
        pdu += struct.pack(">BB",
                           ((self._mesh.iv_index & 0x01) << 7) | meta["netkey"].nid & 0x7f,
                           ((meta["ctl"] & 0x01) << 7) | meta["ttl"] & 0x7f)
        pdu += struct.pack(">I", meta["seq"])[1:]
        pdu += struct.pack(">HH", meta["src"], meta["dst"])
        pdu += lower_transport_pdu
        self._logger.debug("TransportPDU: %s", pdu.hex())
        pdu = meta["netkey"].encrypt(meta, pdu)
        self._logger.debug("preObfuscation: %s", pdu.hex())
        pdu = meta["netkey"].obfuscate(meta, pdu)
        self._logger.debug("NetworkPDU: %s", pdu.hex())
        self.tx_if(meta, pdu)
