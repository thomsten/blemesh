# This file is part of blemesh.
#
# blemesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blemesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with blemesh.  If not, see <http://www.gnu.org/licenses/>.

import logging
import enum
import struct


class Opcode(enum.IntEnum):
    def __init__(self, opcode, company_id=None):
        if ((((opcode & 0x80) == 0 and company_id is None) or
             ((opcode & 0xc000) == 0x8000 and company_id is None) or
             ((opcode & 0xc000) == 0xc000 and company_id is not None))):
            self.opcode = opcode
            self.company_id = company_id
        else:
            raise ValueError("Invalid format for (opcode, company_id): (%r, %r)" %
                             (hex(opcode), company_id))

    def pack(self):
        if self.company_id:
            return struct.pack(">BH", self.opcode, self.company_id)
        elif self.opcode > 0xff:
            return struct.pack(">H", self.opcode)
        else:
            return struct.pack(">B", self.opcode)

    @classmethod
    def unpack(cls, data):
        if (data[0] & 0x80) == 0:
            return cls(*struct.unpack(">B", data[0]))
        elif (data[0] & 0xc0) == 0x80 and len(data) > 1:
            return cls(*struct.unpack(">H", data[:2]))
        elif (data[0] & 0xc0) == 0xc0 and len(data) > 2:
            return cls(*struct.unpack(">BH", data[:3]))
        else:
            raise ValueError("Could not unpack opcode in data: %s" % data.hex())


class Element(object):
    def __init__(self):
        self._models = []

    def model_add(self):
        if model in self._models:
            raise ValueError("Only one model instance allowed at any single element (%r)" % (model))

        self._models.append(model)


class Model(object):
    def __init__(self, opcode_callbacks, model_id, company_id=None):
        self._model_id = model_id
        self._company_id = company_id

    def __eq__(self, other):
        return (self._model_id == other._model_id and
                self._company_id == other._comany_id)

    def __neq__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        if self._company_id:
            return "(%s, %s)" % (self._model_id, self._company_id)
        else:
            return "%s" % (self._model_id)

    def __repr__(self):
        return "Model: %s" % (self.__str__())


class Access(object):
    def __init__(self, mesh, element_count=1, name="Access"):
        self.logger = logging.getLogger(name)
        self._mesh = mesh
        self._elements = [Element(mesh) for i in range(element_count)]

    def model_add(self, element_index=0):
        pass
    def tx(self, meta, pdu):
        self.logger.debug("TX: %s", pdu.hex())

    def rx(self, meta, pdu):
        self.logger.debug("RX: %s", pdu.hex())



