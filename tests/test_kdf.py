from .context import blemesh
import blemesh.kdf as kdf


def test_k1():
    k1_n = bytes(bytearray.fromhex("3216d1509884b533248541792b877f98"))
    k1_salt = bytes(bytearray.fromhex("2ba14ffa0df84a2831938d57d276cab4"))
    k1_p = bytes(bytearray.fromhex("5a09d60797eeb4478aada59db3352a0d"))
    # k1_t = bytes(bytearray.fromhex("c764bea25cf9738b08956ea3c712d5af"))
    k1_expected = bytes(bytearray.fromhex("f6ed15a8934afbe7d83e8dcb57fcf5d7"))

    out = kdf.k1(k1_n, k1_salt, k1_p)
    assert(k1_expected == out)


def test_k2():
    k2_n = bytes(bytearray.fromhex("f7a2a44f8e8a8029064f173ddc1e2b00"))
    k2_p = bytes(bytearray.fromhex("00"))
    nid_expected = bytes(bytearray.fromhex("7f"))
    encryption_key_expected = bytes(bytearray.fromhex("9f589181a0f50de73c8070c7a6d27f46"))
    privacy_key_expected = bytes(bytearray.fromhex("4c715bd4a64b938f99b453351653124f"))
    out = kdf.k2(k2_n, k2_p)
    expected = nid_expected + encryption_key_expected + privacy_key_expected
    assert(expected == out)


def test_k3():
    k3_n = bytes(bytearray.fromhex("f7a2a44f8e8a8029064f173ddc1e2b00"))
    expected = bytes(bytearray.fromhex("ff046958233db014"))
    out = kdf.k3(k3_n)
    assert(expected == out)


def test_k4():
    k4_n = bytes(bytearray.fromhex("3216d1509884b533248541792b877f98"))
    expected = 0x38
    out = kdf.k4(k4_n)
    assert(expected == out)
