from .context import blemesh
import blemesh.keys as keys
from .sample_data import *


def test_netkey_secmat_derivation():
    # From section 8.2.2, p. 282
    nk = keys.NetworkKey(NETKEY)
    assert(NID == nk.nid)
    assert(ENCRYPTION_KEY == nk.encryption_key)
    assert(PRIVACY_KEY == nk.privacy_key)
    assert(NETWORK_ID == nk.network_id)
    assert(IDENTITY_KEY == nk.identity_key)
    assert(BEACON_KEY == nk.beacon_key)


def test_appkey_secmat_derivation():
    ak = keys.ApplicationKey(APPKEY)
    assert(ak.aid == AID)
