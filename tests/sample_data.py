# Bluetooth Mesh profile v1.0 sample data
# https://www.bluetooth.com/specifications/mesh-specifications
from .context import blemesh

APPKEY = bytearray.fromhex("63964771734fbd76e3b40519d1d94a48")
NETKEY = bytearray.fromhex("7dd7364cd842ad18c17c2b820c84c3d6")
DEVKEY = bytearray.fromhex("9d6dd0e96eb25dc19a40ed9914f8f03f")

NID = 0x68
AID = 0x26
ENCRYPTION_KEY = bytearray.fromhex("0953fa93e7caac9638f58820220a398e")
PRIVACY_KEY = bytearray.fromhex("8b84eedec100067d670971dd2aa700cf")
NETWORK_ID = bytearray.fromhex("3ecaff672f673370")
IDENTITY_KEY = bytearray.fromhex("84396c435ac48560b5965385253e210c")
BEACON_KEY = bytearray.fromhex("5423d967da639a99cb02231a83f7d254")
