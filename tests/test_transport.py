from .context import blemesh
from .sample_data import *
import blemesh.keys as keys
import blemesh.transport as transport

ACCESS_PAYLOAD = bytearray.fromhex("04000000010703")

IV_INDEX = 0x12345678
TTL = 0x03
SEQ = 0x0000009
SRC = 0x1201
DST = 0xffff
APPLICATION_NONCE = bytearray.fromhex("01000000091201ffff12345678")
ENC_ACCESS_PAYLOAD = bytearray.fromhex("ca6cd88e698d12")
TRANS_MIC = bytearray.fromhex("65f43fc5")
UPPER_TRANSPORT_PDU = bytearray.fromhex("ca6cd88e698d1265f43fc5")
LOWER_TRANSPORT_PDU = bytearray.fromhex("66ca6cd88e698d1265f43fc5")
SEG = 0

class NetMock(object):

    def __init__(self):
        self.expected_tx = bytearray()

    def tx(self, meta, pdu):
        print("Payload: %s" % (pdu.hex()))
        assert(self.expected_tx == pdu)

class AccessMock(object):
    def rx(self, meta, pdu):
        pass

class MeshMock(object):

    iv_index = IV_INDEX
    seq = SEQ


def test_transport_encrypt():
    ak = keys.ApplicationKey(APPKEY)
    meta = {}
    meta["appkey"] = ak
    meta["mic_len"]= 4
    meta["ttl"] = TTL
    meta["seg"] = SEG
    meta["src"] = SRC
    meta["dst"] = DST
    meta["ctl"] = 0
    net = NetMock()
    net.expected_tx = LOWER_TRANSPORT_PDU
    trs = transport.Transport(MeshMock())
    trs._net = net
    trs.tx(meta, ACCESS_PAYLOAD)
