# This file is part of blemesh.

# blemesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# blemesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with blemesh.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

# with open('README.org') as f:
#     readme = f.read()

# with open('LICENSE') as f:
    # license = f.read()

setup(
    name='blemesh',
    version='0.0.1',
    description='Bluetooth Mesh Python stack',
    # long_description=readme,
    author='Thomas Stenersen',
    author_email='stenersen.thomas@gmail.com',
    # license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
