# BLE Mesh

**NOTE:** This project is very immature and is a work-in-progress.


## Overview

This project implements the Bluetooth Mesh Profile v1.0 in Python.


## Where to read more

-   <https://www.bluetooth.com/specifications/mesh-specifications>


## License

See the `LICENSE` file.

